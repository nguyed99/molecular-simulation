import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.special import factorial



zeta_1 = 10
zeta_2 = 100
moves = 10**6


# c
def MC(zeta: float, no_moves: int, no_ins_del: int = 1) -> np.array:
    """
    no_moves: number of MC moves
    no_ins_del: number of particles to be inserted / deleted per MC move
    """
    N = np.zeros(moves+1, dtype=int)
    N[0] = 0

    for j in range(no_moves):
        N[j+1] = N[j]

        if np.random.uniform() <= 0.5:
            n = 1
            for i in range(1,no_ins_del+1):
                n *= N[j]+i

            p_ins = (zeta**no_ins_del) / n
            if np.random.uniform() < p_ins:
                N[j+1] = N[j] + no_ins_del
          
        else:
            if N[j] != 0:
                n = 1
                for i in range(no_ins_del):
                    n*= N[j]-i
                p_del = n / (zeta**no_ins_del)
                if np.random.uniform() < p_del:
                    N[j+1] = N[j] - no_ins_del
               
        
    return N

N = MC(zeta_1, moves)
print(f'Mean of N: {np.average(N)}')
print(f'Variance of N: {np.var(N)}')

hist_N, N_range = np.histogram(N, bins=np.max(N))
N_range = np.linspace(N_range[0], N_range[-2])
poisson_dist = zeta_1**N_range * np.exp(-zeta_1)/factorial(N_range)

plt.figure()
plt.plot(hist_N/moves, ls='-.', label='Histogram')
plt.plot(N_range, poisson_dist, ls='--', label='Poisson')
plt.legend()
plt.xlabel('N')
plt.tight_layout()
plt.savefig('Problem8_c.png')


# # d - part 1
# no_ins_del = 3

# print(f'Number of particles after {moves=} for {zeta_1=}: {MC(0,zeta_1, moves, no_ins_del)}')
# print(f'Number of particles after {moves=} for {zeta_2=}: {MC(0,zeta_2, moves, no_ins_del)}')

