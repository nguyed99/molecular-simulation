import numpy as np
from tqdm import tqdm


N = 48
k = 1 # spring constant
m = 1
gamma = 0.2 
k_BT = 0.1
d = 3
r_max = 1
eps = 1

## a)
def BAOAB(force, x0, p0, m, dt, N):
    assert(x0.shape == p0.shape)

    x = np.zeros((len(t), *x0.shape))
    p = np.zeros((len(t), *p0.shape))
    x[0] = x0
    p[0] = p0
    xi_2 = np.sqrt(k_BT*(1 - np.exp(-2 * gamma * dt)))

    for i in range(1, len(t)):
        r = np.random.normal(0,1,(N,d))
        p[i] = p[i-1] + 1/2 * force(x[i-1]) * dt # same
        x[i] = x[i-1] + p[i] / (2*m) * dt # diff
        p[i] = np.exp(-gamma*dt) * p[i] + xi_2 * r * np.sqrt(m)
        x[i] = x[i] + (dt/2) * p[i]/m
        p[i] = p[i] + 1/2 * force(x[i]) * dt

    return x, p

# 1D
def FENE(x):
    r1 = x[:3]
    r2 = x[3:]

    f1 = -epsilon * (r1 - r2) / (a**2 - np.linalg.norm(r1 - r2)**2)
    f2 = -f1

    return np.array([
        f1,
        f2,
    ]).flatten()


## b) 

def BAOAB(force, x0, p0, m, dt, L):
    assert(x0.shape == p0.shape)

    x = np.zeros((len(t), *x0.shape))
    p = np.zeros((len(t), *p0.shape))
    U = np.zeros(len(t))

    x[0] = x0
    p[0] = p0
    xi_2 = np.sqrt(k_BT*(1 - np.exp(-2 * gamma * dt)))

    f, U[0] = force(x[0], L)

    for i in tqdm(range(1, len(t))):
        r = np.random.normal(0,1,(N,d))
        p[i] = p[i-1] + 1/2 * f * dt
        x[i] = x[i-1] + p[i] / (2*m) * dt
        p[i] = np.exp(-gamma*dt) * p[i] + xi_2 * r/np.sqrt(m)
        x[i] = x[i] + (dt/2) * p[i]/m

        # enforce periodic boundaries
        x[i] -= (x[i] > L / 2) * L
        x[i] += (x[i] < -L / 2) * L

        f, U[i] = force(x[i], L)
        p[i] = p[i] + 1/2 * f * dt

    return x, p, U




##### noch mal anschauen
def fene(r, L):
    dr = r[1-] - r[:-1]
    dr -= (dr > L/2) * L
    dr += (dr < -L/2) * L
    assert np.all(np.abs(dr) <= L/2)

    dr_abs = np.linalg.norm(dr, axis=1)
    
    fval = -K*dr / (1-(dr_abs / r_max)**2)[:,None]

    f = np.zeros(r.shape)
    f[:-1] = -fval
    f[1:] += fval

    U = -1/2 * K * r_max**2 *np.sum(np.log(1 - (dr_abs / r_max)**2))

    return f, U

def fene_chain(r, L):
    dr = np.roll(r, -1, axis=0) -r
    dr -= (dr > L/2) * L
    dr += (dr < -L/2) * L
    assert np.all(np.abs(dr) <= L/2)

    dr_abs = np.linalg.norm(dr, axis=1)
    
    fval = -K*dr / (1-(dr_abs / r_max)**2)[:,None]

    f = np.zeros(r.shape)
    f = -fval.copy()
    f += np.roll(fval, 1, axis=0)

    U = -1/2 * K * r_max**2 *np.sum(np.log(1 - (dr_abs / r_max)**2))

    return f, U

# R_g = 1.3
# R_l = 3.3
# c_v = 2.5