import numpy as np
import random
import itertools
import matplotlib.pyplot as plt
from tqdm import tqdm

plt.rcParams['axes.grid'] = True
# plt.rcParams.update({
#     "text.usetex": True,
#     "font.family": "sans-serif",
#     "font.sans-serif": "Helvetica",
# })

# # Problem 1
rho = 0.4
sig = 1
eps = 1
k_BT = 1.5 * eps
beta = 1 / k_BT
N = 125
d=3
L = 6.786 * sig
m = 1
tau = np.sqrt(m * sig**2 / eps)

# lattice
n = int(np.round(N**(1/3)))
a = L/n
x = np.linspace(-(n-1) / 2, (n-1) / 2, n)

# initial conditions
x0 = a * np.array(list(itertools.product(x, repeat=3)))
p0 = np.random.normal(0, np.sqrt(m * k_BT), (N, d))

# temporal info
dt = 0.0005 * tau 


def verlet_pot(force, x0, p0, m, dt, t, L):
    assert(x0.shape == p0.shape)

    x = np.zeros((t, *x0.shape))
    p = np.zeros((t, *p0.shape))
    f = np.zeros((t, *x0.shape))
    U = np.zeros(t)

    x[0] = x0
    p[0] = p0

    f[0], U[0] = force(x[0], L)

    for i in tqdm(range(1, t)):
        p[i] = p[i-1] + 1/2 * f[i-1] * dt
        x[i] = x[i-1] + p[i] / m * dt

        # enforce periodic boundaries
        x[i] -= (x[i] > L / 2) * L
        x[i] += (x[i] < -L / 2) * L

        f[i], U[i] = force(x[i], L)
        p[i] = p[i] + 1/2 * f[i] * dt

    return x, p, U, f

def LJ(r, L):
    N = len(r)
    f = np.zeros(r.shape)
    U = 0

    sig6 = sig**6

    for i in range(1, N):
        dr = r[i:] - r[:N-i]
        dr -= (dr > L / 2) * L
        dr += (dr < -L / 2) * L

        r2 = 1 / np.sum(dr**2, axis=1) # scalar product per element
        r6 = r2 * r2 * r2

        U += 4 * eps * sig6 * np.sum(r6 * (sig6 * r6 - 1))
        fval = np.transpose(-24 * eps * sig6 * r6 * r2 * (2 * sig6 * r6 - 1) * dr.T) # cursed scalar multiplication

        f[:N-i] += fval
        f[i:] -= fval

    return f, U

def verlet_thermostat(
    force,
    r0,
    p0,
    m,
    dt,
    t,
    L,
    N_T
):
    assert(r0.shape == p0.shape)

    r = np.zeros((t, *r0.shape))
    p = np.zeros((t, *p0.shape))
    f = np.zeros((t, *r0.shape))
    U = np.zeros(t)

    r[0] = r0
    p[0] = p0

    f[0], U[0] = force(r[0], L)

    for i in tqdm(range(1, t)):
        # first half step
        p[i] = p[i-1] + 1/2 * f[i-1] * dt
        r[i] = r[i-1] + p[i] / m * dt

        # enforce periodic boundaries
        r[i] -= (r[i] > L / 2) * L
        r[i] += (r[i] < -L / 2) * L

        # recompute force
        f[i], U[i] = force(r[i], L)
        # second half step
        p[i] = p[i] + 1/2 * f[i] * dt

        # thermostat (every N_T steps):
        if i % N_T == 0:
            p[i] = np.random.normal(0, np.sqrt(m * k_BT), p0.shape)

    return r, p, U, f

def LJ_pair(r, r_new, L):
    sig6 = sig**6

    dr = r - r_new
    # periodic boundary conditions
    dr -= (dr > L / 2) * L
    dr += (dr < -L / 2) * L
    r2 = 1 / np.sum(dr**2, axis=1) # scalar product per element
    r6 = r2 * r2 * r2

    U = 4 * eps * sig6 * np.sum(r6 * (sig6 * r6 - 1))

    return U

# a) p=2 stimmt übereinander für alle delta_t
# for k in range(4):
#     dt_prime = 2**k * dt
#     t = np.arange(0,10**3*dt, dt_prime)
#     r, p, U, f = verlet_pot(LJ, x0, p0, m, dt_prime, len(t), L)
#     E = 1 / (2 * m) * np.sum(p**2, axis=(1,2)) + U
#     delta_E= E - E[0]
#     plt.figure()
#     plt.plot(t, delta_E / (dt_prime)**2 )

#     plt.ylabel(r'$\frac{\Delta E(t)}{(\Delta t)} / \frac{\epsilon}{\tau}$')
#     plt.xlabel(r'$t / \tau$')
#     plt.tight_layout()
#     plt.savefig(f'problem1a_{k}_order_2.png')

# b)
t_max = 10**3
t = np.arange(0,t_max*dt, dt)
r_bef, p_bef, U, f = verlet_pot(LJ, x0, p0, m, dt, t_max, L)
# E = 1 / (2 * m) * np.sum(p_bef**2, axis=(1,2)) + U

# # root mean square deviation of energy
# rmsq_dev_E = np.sqrt(np.mean((E-E[0])**2) / np.mean(E)**2)
# # root mean square deviation of Impuls
rmsq_dev_p = np.sqrt(np.mean(np.linalg.norm(p_bef - p_bef[0])**2))
deta_p =np.abs(p_bef - p_bef[0])
dev_p = np.einsum('...i,...i->...', deta_p, deta_p)
rmsq_dev_p = np.sqrt(np.mean(dev_p))
# print(f'{rmsq_dev_E=}')
rmsq_dev_p_2 = (np.mean(np.linalg.norm(p_bef-p_bef[0], axis=(1,2))**2))**(1/2)
rmsq_dev_p_2 = (np.mean(np.linalg.norm(np.sum(p_bef-p_bef[0],axis=1), axis=1)**2))**(1/2)

print(f'{rmsq_dev_p=}')
print(f'{rmsq_dev_p_2=}')

# delta_p = np.sum(p_bef - p_bef[0], axis=1)
# plt.figure()
# plt.plot(t,delta_p[:,0], label=r'$\alpha=x$')
# plt.plot(t,delta_p[:,1], label=r'$\alpha=y$')
# plt.plot(t,delta_p[:,2], label=r'$\alpha=z$')
# plt.xlabel(r'$t / \tau$')
# plt.ylabel(r'$\delta p / \frac{\tau \epsilon}{\sigma}$')
# plt.legend()
# plt.tight_layout()
# plt.savefig(f'problem1b.png')



# # c) 
# ## Determine equilibration time
# plt.figure()
# plt.plot(t, E)
# plt.show()




# ## Pressure 
t_eq = 100
t_max = 10**4

r,p,U,f = verlet_thermostat(LJ, r_bef[t_eq], p0, m, dt, t_max,L,int(1/dt))
V = np.sum(r*f, axis=(1,2))
E_kin = 1 / (2 * m) * np.sum(p**2, axis=(1,2))
# plt.figure()
# plt.plot(np.arange(0,t_max,t_max/len(V)), V)
# plt.xlabel(r'$t / \tau$')
# plt.ylabel(r'$V / \epsilon \tau$')
# plt.tight_layout()
# plt.savefig('virial_function_1_protocol.pdf')


# no_of_simulations = 6
# p_ensemble = np.zeros(no_of_simulations)
# std_p_ensemble = np.zeros(no_of_simulations)
# U_ensemble = np.zeros(no_of_simulations)
# std_U_ensemble = np.zeros(no_of_simulations)
# mu_ensemble = np.zeros(no_of_simulations)
# std_mu_ensemble = np.zeros(no_of_simulations)

# ## for different ensembles with different seeds !
# for i in range(no_of_simulations):
#     random.seed(i)
#     p0 = np.random.normal(0, np.sqrt(m * k_BT), (N, d))    
#     r,p,U,f = verlet_thermostat(LJ, r_bef[t_eq], p0, m, dt, t_max,L,int(1/dt))
#     V = np.sum(r*f, axis=(1,2))
#     p_samples = rho*k_BT + (1 / (3 * L**3))* V
#     U_by_N = U/N

#     p_ensemble[i] = np.mean(p_samples)
#     std_p_ensemble[i] = np.std(p_samples)
#     U_ensemble[i] = np.mean(U_by_N)
#     std_U_ensemble[i] = np.std(U_by_N)

#     #mu_ex
#     k = 5
#     r_test = r[int(1 / dt)::int(3 / dt)]

#     samples = np.zeros((len(r_test), 10**k))
#     for y, x in enumerate(r_test):
#         for j in range(10**k):
#             # while np.exp(-beta * LJ_pair(x, np.random.uniform(-L/2, L/2, d), L)) == 0:
#             samples[y,j] = np.exp(-beta * LJ_pair(x, np.random.uniform(-L/2, L/2, d), L))

#     mean = np.mean(samples)
#     std = np.std(samples)

    # mu = - np.log(mean) / beta
    # std_mu = np.abs(1 / (mean * beta)) * std
    # print(f'{mu=}')
    # print(f'{std_mu=}')

# a = [-1.851747546190128, -1.878954082130441, -1.9081508511080745]
# a_std = [5.412283418498762, 5.93628399967974, 5.53353222778447] 
#     mu_ensemble[i] = - np.log(mean) / beta
#     std_mu_ensemble[i] = np.abs(1 / (mean * beta)) * std


# print(f'Pressure at equilibrium: {np.mean(p_ensemble)} +/- {np.std(std_p_ensemble)*100/np.mean(p_ensemble)} %')
# print(f'Internal energy per particle at equilibrium: {np.mean(U_ensemble)} +/- {np.std(std_U_ensemble)*100/np.mean(U_ensemble)} %')
# print(f'Excess chemical potential at equilibrium: {np.mean(mu_ensemble)} +/- {np.std(std_mu_ensemble)*100/np.mean(mu_ensemble)} %')
# print(f'Excess chemical potential at equilibrium_2: {np.mean(mu_ensemble)} +/- {np.std(mu_ensemble)*100/np.mean(mu_ensemble)} %')


## mu_ex


# r_test = r[int(1 / dt)::int(3 / dt)]

# k = 4
# samples = np.zeros((len(r_test), 10**k))
# for i, x in enumerate(r_test):
#     for j in range(10**k):
#         samples[i,j] = np.exp(-beta * LJ_pair(x, np.random.uniform(-L/2, L/2, d), L))


# mean = np.mean(samples, axis=1)
# std = np.std(mean)

# mu = - np.log(np.mean(mean)) / beta
# dmu = np.abs(1 / (np.mean(mean) * beta)) * std
# print(f'Excess chemical potential at equilibrium: {mu} +/- {dmu}')


# mean = np.mean(samples)
# std = np.std(samples)
# dmu = np.abs(1 / (mean * beta)) * std


# mu = - np.log(mean) / beta

# print(f'Excess chemical potential at equilibrium: {mu} +/- {dmu*100/mu}')


