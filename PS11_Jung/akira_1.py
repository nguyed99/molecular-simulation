import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

N = 48
K = 1 # spring constant
m = 1
gamma = 0.2 
k_BT = 0.1
d = 3
r_max = 1
eps = 1
dt = 0.0005
t_max = 10**6*dt

t = np.arange(0,t_max,dt)
M = N*m

# def BAOAB(force, x0, p0, m, dt, N):
#     assert(x0.shape == p0.shape)

#     x = np.zeros((len(t), *x0.shape))
#     p = np.zeros((len(t), *p0.shape))
#     U = np.zeros(len(t))

#     x[0] = x0
#     p[0] = p0
#     xi_2 = np.sqrt(k_BT*(1 - np.exp(-2 * gamma * dt)))

#     f, U[0] = force(x[0])

#     for i in tqdm(range(1, len(t))):
#         r = np.random.normal(0,1,(N,d))
#         p[i] = p[i-1] + 1/2 * f * dt # same
#         x[i] = x[i-1] + p[i] / (2*m) * dt # diff
#         p[i] = np.exp(-gamma*dt) * p[i] + xi_2 * r * np.sqrt(m)
#         x[i] = x[i] + (dt/2) * p[i]/m

#         f, U[i] = force(x[i])
#         p[i] = p[i] + 1/2 * f * dt

#     return x, p, U


# def fene(r):
#     dr = r[1:] - r[:-1]
#     dr_abs = np.linalg.norm(dr, axis=1)

#     fval = -K*dr / (1-(dr_abs / r_max)**2)[:,np.newaxis]

#     f = np.zeros(r.shape)
#     f[:-1] += -fval
#     f[1:] += fval

#     U = -1/2 * K * r_max**2 *np.sum(np.log(1 - (dr_abs / r_max)**2))

#     return f, U


# r0= np.zeros((N,3))
# r0[:, 0] = 0.5*np.arange(N)
# p0 = np.random.normal(0, np.sqrt(m * k_BT), (N, d))

# r, p, U = BAOAB(fene, r0,p0, m, dt, N)
# E = 1 / (2 * m) * np.sum(p**2, axis=(1,2)) + U
# plt.plot(t,E)
# plt.show()

# t_eq = 100
# t_eq = int(t_eq/dt)
# r_cm = 1/M * np.sum(m * r, axis=1)
# r_cm = np.tile(r_cm[:, np.newaxis, :], (1, N, 1))
# R_g = (1/M * np.sum(np.linalg.norm(r-r_cm, axis=2)**2, axis=1))**(1/2)
# R_g_eq = np.mean(R_g[t_eq:])
# d_R_g_eq = 0.5 / R_g_eq * np.std((1/M * np.sum(np.linalg.norm(r-r_cm, axis=2)**2, axis=1))**(1/2))

# R_e_eq = np.mean(np.linalg.norm(r[t_eq:,-1] - r[t_eq:,0], axis=1)**2)**(1/2)
# d_R_e_eq = 0.5 / R_e_eq * np.std(np.linalg.norm(r[t_eq:,-1] - r[t_eq:,0], axis=1)**2)

# T = 1 / (2 * m) * np.sum(p**2, axis=(1, 2))

# H = U[t_eq:] + T[t_eq:]
# dH = H - np.mean(H)
# c_V = np.mean(dH**2) / (N * k_BT**2)
# dc_V = np.std(dH**2) / (N * k_BT**2)


# print(f'R_g at equilibrium: {R_g_eq} +/- {d_R_g_eq*100/R_g_eq} %')
# print(f'R_e at equilibrium: {R_e_eq} +/- {d_R_e_eq*100/R_e_eq} %')
# print(f"c_V = {c_V} ± {dc_V*100/c_V} k_B")


# b)
L = N * r_max/3

def fene_chain(r, L):
    dr = np.roll(r, -1, axis=0) -r
    dr -= (dr > L/2) * L
    dr += (dr < -L/2) * L
    assert np.all(np.abs(dr) <= L/2)

    dr_abs = np.linalg.norm(dr, axis=1)
    
    fval = -K*dr / (1-(dr_abs / r_max)**2)[:,None]

    f = np.zeros(r.shape)
    f = -fval.copy()
    f += np.roll(fval, 1, axis=0)

    U = -1/2 * K * r_max**2 *np.sum(np.log(1 - (dr_abs / r_max)**2))

    return f, U

def BAOAB_pbc(force, x0, p0, m, dt, L):
    assert(x0.shape == p0.shape)

    x = np.zeros((len(t), *x0.shape))
    p = np.zeros((len(t), *p0.shape))
    U = np.zeros(len(t))

    x[0] = x0
    p[0] = p0
    xi_2 = np.sqrt(k_BT*(1 - np.exp(-2 * gamma * dt)))

    f, U[0] = force(x[0], L)

    for i in tqdm(range(1, len(t))):
        r = np.random.normal(0,1,(N,d))
        p[i] = p[i-1] + 1/2 * f * dt
        x[i] = x[i-1] + p[i] / (2*m) * dt
        # enforce periodic boundaries
        x[i] -= (x[i] > L / 2) * L
        x[i] += (x[i] < -L / 2) * L

        p[i] = np.exp(-gamma*dt) * p[i] + xi_2 * r/np.sqrt(m)
        x[i] = x[i] + (dt/2) * p[i]/m

        # enforce periodic boundaries
        x[i] -= (x[i] > L / 2) * L
        x[i] += (x[i] < -L / 2) * L

        f, U[i] = force(x[i], L)
        p[i] = p[i] + 1/2 * f * dt

    return x, p, U

r0_0= np.zeros((N,3))
r0_0[:, 0] = 0.5*np.arange(N)
p0_0 = np.random.normal(0, np.sqrt(m * k_BT), (N, d))

r, p, U = BAOAB_pbc(fene_chain, r0_0,p0_0, m, dt, N)



## mean square displacement
msdv_0 = np.zeros(len(r))
for i in tqdm(range(1, len(msdv_0))):
    # msdv_0[i] = np.mean(np.sum((r[i:] - r[:-i])**2, axis=2))
    msdv_0[i] = np.mean(np.linalg.norm(r[i:] - r[:-i], axis=2)**2)    


N=96
r0_1= np.zeros((N,3))
r0_1[:, 0] = 0.5*np.arange(N)
p0_1 = np.random.normal(0, np.sqrt(m * k_BT), (N, d))

r1, p1, U1 = BAOAB_pbc(fene_chain, r0_1,p0_1, m, dt, N)
msdv_1 = np.zeros(len(r1))
for i in tqdm(range(1, len(msdv_1))):
    # msdv_1[i] = np.mean(np.sum((r1[i:] - r1[:-i])**2, axis=2))
    msdv_1[i] = np.mean(np.linalg.norm(r1[i:] - r1[:-i], axis=2)**2)    

plt.figure()
plt.loglog(t, msdv_0, label=r'$N=48$')
plt.loglog(t, msdv_1, label=r'$N=96$')

plt.xlabel(r'$\delta r^2(t) / r_{max}^{2}$')
plt.ylabel(r'$t / \tau$')
plt.legend()
plt.tight_layout()
plt.savefig('2b_N_48.pdf')