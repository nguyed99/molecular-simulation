import itertools
from typing import Callable

import matplotlib.pyplot as plt
import numpy as np
from numpy.typing import ArrayLike
from scipy.constants import Boltzmann as k_B
from tqdm import tqdm

## a
N = 64 # number of particles
d = 3 # number of dimensions
m0 = 1 # mass of particles
sig = 1 # sigma (length scale)
eps = 1 # epsilon (energy scale)
tau = np.sqrt(m0 * sig**2 / eps) # time scale
L = 6 * sig # length of simulation box
m = np.full((N, d), m0) # mass of all particles
a = 1.5 * sig # lattice constant
k_BT = 1.5 * eps # temperature
beta = 1 / k_BT
dt = 0.002 * tau # timestep

# create lattice
n = int(np.round(N**(1/3)))
x = np.linspace(-(n-1) / 2, (n-1) / 2, n)
r0 = a * np.array(list(itertools.product(x, repeat=3)))

p0 = np.random.normal(0, np.sqrt(m0 * k_BT), r0.shape)

t_max = 500 * tau
t = np.arange(0, t_max, dt)

def verlet_thermostat(
    force: Callable,
    r0: ArrayLike,
    p0: ArrayLike,
    m: ArrayLike,
    dt: float,
    N: int,
    L: float,
    T: float,
    N_T: int
) -> (ArrayLike, ArrayLike, ArrayLike):
    assert(r0.shape == p0.shape)
    assert(r0.shape == m.shape)

    r = np.zeros((N, *r0.shape))
    p = np.zeros((N, *p0.shape))
    U = np.zeros(N)

    r[0] = r0
    p[0] = p0

    f, U[0] = force(r[0], L)

    for i in tqdm(range(1, N)):
        # first half step
        p[i] = p[i-1] + 1/2 * f * dt
        r[i] = r[i-1] + p[i] / m * dt

        # enforce periodic boundaries
        r[i] -= (r[i] > L / 2) * L
        r[i] += (r[i] < -L / 2) * L

        # recompute force
        f, U[i] = force(r[i], L)
        # second half step
        p[i] = p[i] + 1/2 * f * dt

        # thermostat (every N_T steps):
        if i % N_T == 0:
            p[i] = np.random.normal(0, np.sqrt(m.item(0) * T), p0.shape)

    return r, p, U

def LJ(r: ArrayLike, L: float) -> (ArrayLike, float):
    N = len(r)
    f = np.zeros(r.shape)
    U = 0

    sig6 = sig**6

    for i in range(1, N):
        dr = r[i:] - r[:N-i]
        # periodic boundary conditions
        dr -= (dr > L / 2) * L
        dr += (dr < -L / 2) * L

        r2 = 1 / np.sum(dr**2, axis=1) # scalar product per element
        r6 = r2 * r2 * r2

        U += 4 * eps * sig6 * np.sum(r6 * (sig6 * r6 - 1))
        fval = np.transpose(-24 * eps * sig6 * r6 * r2 * (2 * sig6 * r6 - 1) * dr.T) # cursed scalar multiplication

        f[:N-i] += fval
        f[i:] -= fval

    return f, U


# Widom's method
no_of_eq_config = int(30 / dt)
r, p, U = verlet_thermostat(LJ, r0, p0, m, dt, int(60 / dt), L, k_BT, int(0.1 / dt))
r = r[int(30 / dt):]
p = r[int(30 / dt):]
# exp_delta_U = np.zeros(no_of_eq_config)

# for i in range(no_of_eq_config):
#     # insert particle
#     r_new = np.random.uniform(-3, 3,3).T
#     # draw a random equilibrium config
#     config_idx = np.random.randint(0,15000)
#     U_old = U[config_idx]
#     r_new = np.vstack((r[config_idx], r_new))
#     _, U_new = LJ(r_new, L)
#     exp_delta_U[i] = np.exp(-(1/k_BT)*(U_new-U_old))

# mu_ex = -np.log(np.average(exp_delta_U))*(k_BT)
# print(f'{mu_ex=}') # mu_ex=-40.73901237289709
# std = np.std(exp_delta_U)
# dmu = np.abs(1/(np.average(exp_delta_U)/(k_BT))) * std
# print(f'{np.abs(dmu / mu_ex)=}')

# # 
k = [1,2,3]
mu_ex_k = np.zeros(3)
mu_ex_k_err = np.zeros(3)
for k_i in tqdm(k):
    exp_delta_U = np.zeros(100)
    for j in range(100): # 100 indepdent equilibrium configuration
        exp_delta_U_temp = np.zeros(10**k_i)
        config_idx = np.random.randint(0,15000)
        U_config = U[config_idx]
        r_config = r[config_idx]

        for i in range(10**k_i):
            r_new = np.random.uniform(-3, 3,3).T
            r_config = np.vstack((r_config, r_new))
            _, U_new = LJ(r_config, L)
            exp_delta_U_temp[i] = np.exp(-(1/k_BT)*(U_new-U_config))
            U_config = U_new

        exp_delta_U[j] = np.average(exp_delta_U_temp)

    mu_ex_k[k_i-1] = -np.log(np.average(exp_delta_U))*(k_BT)
    std = np.std(exp_delta_U)
    dmu = np.abs(1/(np.average(exp_delta_U)/(k_BT))) * std
    mu_ex_k_err[k_i-1] = np.abs(dmu / mu_ex_k[k_i])
    print(f'{mu_ex_k[k_i-1]=}')
    print(f'{mu_ex_k_err[k_i-1]=}')




