import itertools
from typing import Callable

import matplotlib.pyplot as plt
import numpy as np
from numpy.typing import ArrayLike
from scipy.constants import Boltzmann as k_B
from tqdm import tqdm

# Problem 6.1
# a)
N = 64 # number of particles
d = 3 # number of dimensions
m0 = 1 # mass of particles
sig = 1 # sigma (length scale)
eps = 1 # epsilon (energy scale)
tau = np.sqrt(m0 * sig**2 / eps) # time scale
L = 6 * sig # length of simulation box
m = np.full((N, d), m0) # mass of all particles
a = 1.5 * sig # lattice constant
k_BT = 2 * eps # temperature
dt = 0.002 * tau # timestep

# create lattice
n = int(np.round(N**(1/3)))
x = np.linspace(-(n-1) / 2, (n-1) / 2, n)
r0 = a * np.array(list(itertools.product(x, repeat=3)))

p0 = np.random.normal(0, np.sqrt(m0 * k_BT), r0.shape)

t_max = 500 * tau
t = np.arange(0, t_max, dt)

def verlet_thermostat(
    force: Callable,
    r0: ArrayLike,
    p0: ArrayLike,
    m: ArrayLike,
    dt: float,
    N: int,
    L: float,
    T: float,
    N_T: int
) -> (ArrayLike, ArrayLike, ArrayLike):
    assert(r0.shape == p0.shape)
    assert(r0.shape == m.shape)

    r = np.zeros((N, *r0.shape))
    p = np.zeros((N, *p0.shape))
    U = np.zeros(N)

    r[0] = r0
    p[0] = p0

    f, U[0] = force(r[0], L)

    for i in tqdm(range(1, N)):
        # first half step
        p[i] = p[i-1] + 1/2 * f * dt
        r[i] = r[i-1] + p[i] / m * dt

        # enforce periodic boundaries
        r[i] -= (r[i] > L / 2) * L
        r[i] += (r[i] < -L / 2) * L

        # recompute force
        f, U[i] = force(r[i], L)
        # second half step
        p[i] = p[i] + 1/2 * f * dt

        # thermostat (every N_T steps):
        if i % N_T == 0:
            p[i] = np.random.normal(0, np.sqrt(m.item(0) * T), p0.shape)

    return r, p, U

def verlet(
    force: Callable,
    r0: ArrayLike,
    p0: ArrayLike,
    m: ArrayLike,
    dt: float,
    N: int,
    L: float,
) -> (ArrayLike, ArrayLike, ArrayLike):
    assert(r0.shape == p0.shape)
    assert(r0.shape == m.shape)

    r = np.zeros((N, *r0.shape))
    p = np.zeros((N, *p0.shape))
    U = np.zeros(N)

    r[0] = r0
    p[0] = p0

    f, U[0] = force(r[0], L)

    for i in tqdm(range(1, N)):
        # first half step
        p[i] = p[i-1] + 1/2 * f * dt
        r[i] = r[i-1] + p[i] / m * dt

        # enforce periodic boundaries
        r[i] -= (r[i] > L / 2) * L
        r[i] += (r[i] < -L / 2) * L

        # recompute force
        f, U[i] = force(r[i], L)
        # second half step
        p[i] = p[i] + 1/2 * f * dt

    return r, p, U

def LJ(r: ArrayLike, L: float) -> (ArrayLike, float):
    N = len(r)
    f = np.zeros(r.shape)
    U = 0

    sig6 = sig**6

    for i in range(1, N):
        dr = r[i:] - r[:N-i]
        # periodic boundary conditions
        dr -= (dr > L / 2) * L
        dr += (dr < -L / 2) * L

        r2 = 1 / np.sum(dr**2, axis=1) # scalar product per element
        r6 = r2 * r2 * r2

        U += 4 * eps * sig6 * np.sum(r6 * (sig6 * r6 - 1))
        fval = np.transpose(-24 * eps * sig6 * r6 * r2 * (2 * sig6 * r6 - 1) * dr.T) # cursed scalar multiplication

        f[:N-i] += fval
        f[i:] -= fval

    return f, U

r, p, U = verlet_thermostat(LJ, r0, p0, m, dt, len(t), L, k_BT, int(0.1 / dt))
T = 1 / (2 * m0) * np.sum(p**2, axis=(1, 2))

# plt.plot(t, T)
# plt.plot(t, U)
# plt.show()

t_eq = 30 * tau
dT = T[int(t_eq / dt):] - np.mean(T[int(t_eq / dt):])
c_V = 3/2 / (1 - 3/2 * N * np.mean((dT / T[int(t_eq / dt):])**2))
dc_V = 3/2 / (1 - 3/2 * N * np.std((dT / T[int(t_eq / dt):])**2))
dU = U[int(t_eq / dt):] - np.mean(U[int(t_eq / dt):])

print(f"<dT²> = {np.average(dT**2)} ± {np.std(dT**2)} ϵ")
print(f"c_V = {c_V} ± {dc_V} k_B")
print(f"<dU²> = {np.average(dU**2)} ± {np.std(dU**2)} ϵ")

# b)
# i)
r, p, U = verlet_thermostat(LJ, r0, p0, m, dt, int(60 / dt), L, k_BT, int(0.1 / dt))

# ii)
H = U + 1 / (2 * m0) * np.sum(p**2, axis=(1, 2))
H_avg = np.average(H[int(30 / dt):])

# 1/2m * (sum p)**2 * s + U = H
# => s = 2m * (H - U) / (sum p)**2
s = 2 * m0 * (H_avg - U[-1]) / np.sum(p[-1]**2)
p_rescaled = np.sqrt(s) * p[-1]

# iii)
r, p, U = verlet(LJ, r[-1], p_rescaled, m, dt, int(60 / dt), L)

T = 1 / (2 * m0) * np.sum(p**2, axis=(1, 2))
temp = 2/3 * np.average(T[int(30 / dt):]) / N

print(f"k_B T = {temp}")

# iv)
r, p, U = verlet(LJ, r[-1], p[-1], m, dt, int(500 / dt), L)
T = 1 / (2 * m0) * np.sum(p**2, axis=(1, 2))

H = U + T
dH = H - np.mean(H)
c_V = np.mean(dH**2) / (N * k_BT**2)
dc_V = np.std(dH**2) / (N * k_BT**2)
print(f"c_V = {c_V} ± {dc_V} k_B")

# c)
k_BT = 1.35

# i)
r, p, U = verlet_thermostat(LJ, r0, p0, m, dt, int(60 / dt), L, k_BT, int(0.1 / dt))

# ii)
H = U + 1 / (2 * m0) * np.sum(p**2, axis=(1, 2))
H_avg = np.average(H[int(30 / dt):])

s = 2 * m0 * (H_avg - U[-1]) / np.sum(p[-1]**2)
p_rescaled = np.sqrt(s) * p[-1]

# iii)
r, p, U = verlet(LJ, r[-1], p_rescaled, m, dt, int(60 / dt), L)

T = 1 / (2 * m0) * np.sum(p**2, axis=(1, 2))
temp = 2/3 * np.average(T[int(30 / dt):]) / N

print(f"k_B T = {temp}")

# iv)
r, p, U = verlet(LJ, r[-1], p[-1], m, dt, int(500 / dt), L)
T = 1 / (2 * m0) * np.sum(p**2, axis=(1, 2))

H = U + T
dH = H - np.mean(H)
c_V = np.mean(dH**2) / (N * k_BT**2)
dc_V = np.std(dH**2) / (N * k_BT**2)
print(f"c_V = {c_V} ± {dc_V} k_B")
