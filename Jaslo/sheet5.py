import itertools
import time

import numpy as np
from scipy.constants import Boltzmann as k_B
import matplotlib.pyplot as plt

def verlet(force, x0, p0, m, dt, N, L):
    assert(x0.shape == p0.shape)

    x = np.zeros((N, *x0.shape))
    p = np.zeros((N, *p0.shape))

    x[0] = x0
    p[0] = p0

    for i in range(1, N):
        p[i] = p[i-1] + 1/2 * force(x[i-1], L) * dt
        x[i] = x[i-1] + p[i] / m * dt

        # enforce periodic boundaries
        x[i] -= (x[i] > L / 2) * L
        x[i] += (x[i] < -L / 2) * L

        p[i] = p[i] + 1/2 * force(x[i], L) * dt

    return x, p

# Problem 5.1
# a)
L = 11
N = 11
k = 1
m = np.full((N, 3), 1)
M = m[0,0] * N
l = 1
eps = k * l**2
tau = np.sqrt(m[0] / k)

def force(x, L):
    f = np.zeros(x.shape)

    dx = x[:-1] - x[1:]
    # periodic reduction
    dx -= (dx > L / 2) * L
    dx += (dx < -L / 2) * L
    f[:-1] += -k * dx

    dx = x[1:] - x[:-1]
    # periodic reduction
    dx -= (dx > L / 2) * L
    dx += (dx < -L / 2) * L
    f[1:] += -k * dx

    return f

I = np.arange(1, N+1)
r0 = np.zeros((N, 3))
r0[:,0] = l * I - L / 2
p0 = np.zeros(r0.shape)
p0[5] = np.array([-3/7, 6/7, -2/7])

dt = 0.1
t_max = 100 # tau
t = np.arange(0, t_max, dt)

r = np.zeros((len(t), *r0.shape))
p = np.zeros((len(t), *p0.shape))

r, p = verlet(force, r0, p0, m, dt, len(t), L)

plt.plot(r[:,0,0], r[:,0,1], label="Particle 1")
plt.plot(r[:,5,0], r[:,5,1], label="Particle 6")
plt.legend()
plt.xlabel(r"$x$")
plt.ylabel(r"$y$")
plt.savefig("problem5.1a-trajectories.png")
plt.close()

r_CM = 1/M * np.sum(m * r, axis=1)
plt.plot(t, r_CM[:,0], label=r"$x$")
plt.plot(t, r_CM[:,1], label=r"$y$")
plt.plot(t, r_CM[:,2], label="$z$")
plt.xlabel(r"$t / \tau$")
plt.ylabel(r"Distance $/ l$")
plt.savefig("problem5.1a-r_CM.png")
plt.close()

def potential(r, L):
    dr = r[:,1:] - r[:,:-1]
    dr -= (dr > L / 2) * L
    dr += (dr < -L / 2) * L

    U = 1/2 * k * np.sum(dr**2, axis=(1,2))

    return U

E_pot = potential(r, L)
E_kin = 1 / (2 * m[0,0]) * np.sum(p**2, axis=(1,2))
E = E_pot + E_kin

plt.plot(t, E)
plt.savefig("problem5.1a-E.png")
plt.xlabel(r"$t / \tau$")
plt.ylabel(r"$E / \epsilon$")
plt.close()

# b)
def force_b(x, L):
    f = np.zeros(x.shape)

    dx = x - np.roll(x, -1, axis=0)
    # periodic reduction
    dx -= (dx > L / 2) * L
    dx += (dx < -L / 2) * L
    f += -k * dx

    dx = x - np.roll(x, 1, axis=0)
    # periodic reduction
    dx -= (dx > L / 2) * L
    dx += (dx < -L / 2) * L
    f += -k * dx

    return f

r, p = verlet(force_b, r0, p0, m, dt, len(t), L)

plt.plot(r[:,0,0], r[:,0,1], label="Particle 1")
plt.plot(r[:,5,0], r[:,5,1], label="Particle 6")
plt.legend()
plt.xlabel(r"$x$")
plt.ylabel(r"$y$")
plt.savefig("problem5.1b-trajectories.png")
plt.close()

r_CM = 1/M * np.sum(m * r, axis=1)
plt.plot(t, r_CM[:,0], label=r"$x$")
plt.plot(t, r_CM[:,1], label=r"$y$")
plt.plot(t, r_CM[:,2], label="$z$")
plt.xlabel(r"$t / \tau$")
plt.ylabel(r"Distance $/ l$")
plt.savefig("problem5.1b-r_CM.png")
plt.close()

def potential(r, L):
    dr = r[:,1:] - r[:,:-1]
    dr -= (dr > L / 2) * L
    dr += (dr < -L / 2) * L

    U = 1/2 * k * np.sum(dr**2, axis=(1,2))

    return U

E_pot = potential(r, L)
E_kin = 1 / (2 * m[0,0]) * np.sum(p**2, axis=(1,2))
E = E_pot + E_kin

plt.plot(t, E)
plt.savefig("problem5.1b-E.png")
plt.xlabel(r"$t / \tau$")
plt.ylabel(r"$E / \epsilon$")
plt.close()

# Problem 5.2
# a)
N = 64
d = 3
sig = 1
eps = 1
L = 6 * sig
m = np.full((N, d), 1)
tau = np.sqrt(m[0,0] * sig**2 / eps)
a = 1.5 * sig
k_BT = 1.5 * eps
dt = 0.002 * tau

# create lattice
n = int(np.round(N**(1/3)))
x = np.linspace(-(n-1) / 2, (n-1) / 2, n)
r0 = a * np.array(list(itertools.product(x, repeat=3)))

p0 = np.random.normal(0, np.sqrt(m[0,0] * k_BT), (N, d))

t_max = 100 * tau
t = np.arange(0, t_max, dt)

def verlet_pot(force, x0, p0, m, dt, N, L):
    assert(x0.shape == p0.shape)

    x = np.zeros((N, *x0.shape))
    p = np.zeros((N, *p0.shape))
    U = np.zeros(N)

    x[0] = x0
    p[0] = p0

    f, U[0] = force(x[0], L)

    for i in range(1, N):
        p[i] = p[i-1] + 1/2 * f * dt
        x[i] = x[i-1] + p[i] / m * dt

        # enforce periodic boundaries
        x[i] -= (x[i] > L / 2) * L
        x[i] += (x[i] < -L / 2) * L

        f, U[i] = force(x[i], L)
        p[i] = p[i] + 1/2 * f * dt

    return x, p, U

def LJ(r, L):
    N = len(r)
    f = np.zeros(r.shape)
    U = 0

    sig6 = sig**6

    # for i in range(N):
    #     for j in range(i+1, N):
    #         dr = r[j] - r[i]
    #         dr -= (dr > L / 2) * L
    #         dr += (dr < -L / 2) * L

    #         r2 = 1 / np.dot(dr, dr)
    #         r6 = r2**3
    #         U += 4 * eps * sig6 * r6 * (sig6 * r6 - 1)
    #         fval = -24 * eps * sig6 * r6 * r2 * (2 * sig6 * r6 - 1) * dr
    #         f[i] += fval
    #         f[j] -= fval

    for i in range(1, N):
        dr = r[i:] - r[:N-i]
        dr -= (dr > L / 2) * L
        dr += (dr < -L / 2) * L

        r2 = 1 / np.sum(dr**2, axis=1) # scalar product per element
        r6 = r2 * r2 * r2

        U += 4 * eps * sig6 * np.sum(r6 * (sig6 * r6 - 1))
        fval = np.transpose(-24 * eps * sig6 * r6 * r2 * (2 * sig6 * r6 - 1) * dr.T) # cursed scalar multiplication

        f[:N-i] += fval
        f[i:] -= fval

    return f, U

r, p, U1 = verlet_pot(LJ, r0, p0, m, dt, len(t), L)
E1 = 1 / (2 * m[0,0]) * np.sum(p**2, axis=(1,2)) + U1

r, p, U2 = verlet_pot(LJ, r0, p0, m, 2 * dt, len(t) // 2, L)
E2 = 1 / (2 * m[0,0]) * np.sum(p**2, axis=(1,2)) + U2

plt.plot(t[::2], E2 - E2[0], label=r"$2 \Delta t$")
plt.plot(t, E1 - E1[0], label=r"$\Delta t$")
plt.legend()
plt.savefig("problem5.2a-E.png")
plt.close()

# b)
# k = np.array([1, 2, 3, 4])
# N = 2**(3 * k)
# T = np.zeros(N.shape)

# for i in range(len(N)):
#     L = 2**k[i] * sig
#     t_max = 10 * tau
#     t = np.arange(0, t_max, dt)
#     m = np.full((N[i], d), 1)
#     n = int(np.round(N[i]**(1/3)))
#     x = np.linspace(-(n-1) / 2, (n-1) / 2, n)
#     r0 = a * np.array(list(itertools.product(x, repeat=3)))
#     p0 = np.random.normal(0, np.sqrt(m[0,0] * k_BT), (N[i], d))

#     T1 = time.time()
#     r, p, U = verlet_pot(LJ, r0, p0, m, dt, len(t), L)
#     T2 = time.time()

#     T[i] = T2 - T1

# plt.plot(N, T)
# plt.xlabel(r"$N$")
# plt.ylabel(r"$T / s$")
# #plt.savefig("problem5.2b.png")
# plt.close()

# c)
N = 64
L = 6 * sig
m = np.full((N, d), 1)
dt = 0.002 * tau

# create lattice
n = int(np.round(N**(1/3)))
x = np.linspace(-(n-1) / 2, (n-1) / 2, n)
r0 = a * np.array(list(itertools.product(x, repeat=3)))

p0 = np.random.normal(0, np.sqrt(m[0,0] * k_BT), (N, d))

t_max = 100 * tau
t = np.arange(0, t_max, dt)

r, p, U = verlet_pot(LJ, r0, p0, m, dt, len(t), L)

V = np.sum(r * p, axis=(1, 2))

plt.plot(t, U / N)
plt.xlabel(r"$t / \tau$")
plt.ylabel(r"$u / \epsilon$")
plt.savefig("problem5.2c-U.png")
plt.close()

plt.plot(t, V / N)
plt.xlabel(r"$t / \tau$")
plt.ylabel(r"$v / \epsilon \tau$")
plt.savefig("problem5.2c-V.png")
plt.close()

t_eq = 40
t_max = 1000
t = np.arange(0, t_max, dt)
r, p, U = verlet_pot(LJ, r0, p0, m, dt, len(t), L)
V = np.sum(r * p, axis=(1, 2))
K = np.sum(p**2 / (2 * m[0][0]), axis=(1,2))

U_a = np.average(U[int(t_eq / dt):]) / N
V_a = np.average(V[int(t_eq / dt):]) / N
temp = 2 * np.average(K[int(t_eq / dt):]) / N

print(f"<U> = {U_a} eps")
print(f"<V> = {V_a}")
print(f"k_B T = {temp} k_B")
