import numpy as np
from graphics import twoD_plot
from pathlib import Path
import os
path = Path(__file__).parent.resolve() / 'Results'
if not os.path.exists(path):
    os.makedirs(path)


# N = 11
l = 1 # equilibrium extension of bonds
tau = 1
k = 1
epsilon = 1
m = 1
t=(0,300)
dt = 1e-2
k_B = 1.380649*1e-23
# x0 = np.zeros((N,3))
# x0[:,0]=np.array([i+1 for i in range(N)])
# p0 = np.zeros((N,3))
# p0[0,:] = np.array([1/2,0,0])
# p0[-1,:]= -p0[0,:]

def force(x: np.array) -> np.array:
    f = np.zeros((1,*x.shape))
    n_d, n_row, n_col = f.shape

    # right neighbor
    for i in range(n_row-1):
        f[0,i,:] += -k*(x[i,:]-x[i+1,:])
    # left neighbor
    for i in range(1,n_row):
        f[0,i,:] += -k*(x[i,:]-x[i-1,:])
    return f
    
def verlet_chain_HO(x0: np.array, p0: np.array, t:tuple, dt: float, f, N):
    """
    :param N: number of HOs
    """

    t = np.arange(t[0], t[1], dt)
    x = np.zeros((len(t),N,3))
    p = np.zeros((len(t),N,3))

    x[0,:,:] = x0
    p[0,:,:] = p0

    for i in range(1,len(t)):
        x[i,:,:] = x[i-1,:,:] + p[i-1,:,:]*dt/m + (1/2)*(f(x[i-1,:,:])/m)*(dt**2)
        p[i,:,:] = p[i-1,:,:] + (1/2)*(f(x[i-1,:,:]))*dt
        x[i,:,:] = x[i-1,:,:] + p[i,:,:]*dt/m 
        p[i,:,:] = p[i,:,:] + (1/2)*f(x[i,:,:])*dt
    
    return x,p

# x,p = verlet_chain_HO(x0,p0,t,dt,force)

# t = np.arange(t[0], t[1], dt)
# for i in range(N):
#     bead_position = np.vstack((t,x[:,i,0]))
#     twoD_plot([bead_position], [f'bead {i+1}'], ['t/$\omega_{0}^{-1}$','$r_{i}$/$\mathcal{l}$'], f'Position of bead {i+1}', f'bead_{i+1}_r' ,path)
    
# b) Temp ?
# for i in [2,3,11,100]:
#     print(f'i: {i}')
#     x0 = np.zeros((i,3))
#     x0[:,0]=np.array([j+1 for j in range(i)])
#     p0 = np.zeros((i,3))
#     p0[0,:] = np.array([1/2,0,0])
#     p0[-1,:]= -p0[0,:]
#     x,p = verlet_chain_HO(x0,p0,t,dt,force, N=i)
#     U = np.zeros((len(t),i,3))
#     diff = np.diff(x,axis=1)
#     U[:,1:,:]=(k/2)*diff**2
#     E_t = p**2/(2*m) + U
#     E_t = np.sum(np.sum(E_t, axis=1)[:,0])/len(t)
#     T = E_t/((1/2)*(2*i-1))
#     print(f'T: {T}')
# c) end-to-end distance
# end_to_end_dist = np.vstack((t,np.abs(x[:,-1,:]-x[:,0,:])[:,0]))
# twoD_plot([end_to_end_dist], axis=['t/$\omega_{0}^{-1}$', '$|\mathbf{r}_{N}-\mathbf{r}_{1}|/l$'], fname='end_to_end',path=path)

# d)recurrent behaviour test for random x-component of the momenta sampled from a Gaussian distribution with var = m*eps

N=[10,20]
eps=k*l**2
t= np.arange(10,2*10**4,dt)
t_10 = t*10
d=[]

def dist(x1: np.ndarray, x2: np.ndarray, p1: np.ndarray, p2:np.ndarray) -> float:
    """
    :param x1, x2: Nx3 position array
    :param p1, p2: Nx3 momentum array
    """
    N, _ = x1.shape
    return np.sqrt((1/(6*N))*np.sum(k*(x1-x2)**2 + (1/m)*(p1-p2)**2, axis=1))

# for chain_len in N:
chain_len = 10
x0 = np.zeros((chain_len,3))
x0[:,0]=np.array([i+1 for i in range(chain_len)])
p0 = np.zeros((chain_len,3))
p0[:,0] = np.random.normal(0,np.sqrt(m*eps),chain_len)
x, p = verlet_chain_HO(x0,p0,(10,2*10**4),1,force, N=chain_len)
x = x[:,:,0]
p = p[:,:,0]

x_10, p_10 = verlet_chain_HO(x0,p0,(100,2*10**5),10,force, N=chain_len)
x_10 = x_10[:,:,0]
p_10 = p_10[:,:,0]
d = dist(x,x_10,p,p_10)
