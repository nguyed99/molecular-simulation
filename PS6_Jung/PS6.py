from PS5_Jung.PS5 import force_LJ, verlet_pot
import itertools
import numpy as np

# # Problem 6.1
# a)

N = 64
sig = 1
L = 6 * sig
eps = 1
m = 1
tau = 1
k_BT = 2
dt = 0.01
tmax = 100

# Lattice setup
a = 1.5 # lattice constant
n = int(np.round(N**(1/3)))
x = np.linspace(-(n-1) / 2, (n-1) / 2, n)
r0 = a * np.array(list(itertools.product(x, repeat=3)))
p0 = np.random.normal(0, np.sqrt(m * k_BT), (N, 3))
t = np.arange(0, tmax, dt)

# stochastic Maxwell-Boltzmann thermostat - coupling interval of 0.1 tau
dn = int(1/0.1) # coupling every dn time steps
r = np.zeros((len(t), N, 3))
p = np.zeros((len(t), N, 3))
U = np.zeros(len(t))
r[:dn], p[:dn], U[:dn] = verlet_pot(force_LJ, r0, p0, m, dt, dn, L) 
for i in range(1,int(tmax/0.1)):
    p[i * dn - 1] = np.random.normal(0, np.sqrt(m * k_BT), (N, 3))
    r[i * dn:(i+1) * dn], p[i * dn:(i+1) * dn], U[i * dn:(i+1) * dn] = verlet_pot(force_LJ, r[i * dn - 1], p[i * dn - 1], m, dt, dn, L)
    


# assume equilibration time of 30 tau
# E_kin fluctuations

# E_kin_time_av = np.average(np.sum(p[int(30/dt):]**2/(2*m)))
# delta_E_kin_square_time_av = np.average(np.square(np.sum(p[int(30/dt):]**2/(2*m), axis=(1,2)) - E_kin_time_av))
E_kin_time_av = 0.5 * np.mean(p[int(30/dt):]**2) 
E_kin_deviation = 0.5 * p[int(30/dt):]**2 - E_kin_time_av
E_kin_mean_square_fluctuation = np.mean(E_kin_deviation**2)

# E_pot fluctuations
# U_pot_time_av = (1/(len(t)-30/dt))*np.sum(U)
# U_t = np.asarray(U_t)
# delta_U_square_time_av = np.average((U_t - U_pot_time_av)**2)
E_pot_time_av = np.mean(U[int(30/dt):])
E_pot_deviation = U[int(30/dt):] - E_pot_time_av
E_pot_mean_square_fluctuation = np.mean(E_kin_deviation**2)
# E_pot_de
# # Problem 6.2
# a)
def box_muller_transformation():
    """
    Function transforms a two-dimensional continuous uniform distribution to a two-dimensional bivariate normal distribution
    """
    while True:
        u, v = np.random.uniform(-1, 1,2)
        q = u**2 + v**2
        if 0<q<1:
            p = np.sqrt(-2*np.log(q)/q)
            return (u*p, v*p)
   
# b)
# from itertools import chain
# k = [1,2,3,4,5]

# for i in k:
#     result = list(chain.from_iterable([box_muller_transformation() for _ in range(int(i/2))]))
#     mean = np.mean(result)
#     standard_error = np.std(result)
#     variance = np.var(result)
#     print(f'N = 10^{i}')
#     print(f'mean: {mean}')
#     print(f'standard error: {standard_error}')
#     print(f'varaince: {variance}')

#     # histogram plotting
#     # expected distribution
#     data = np.random.normal(mu=0, sigma=1, size=10**i)