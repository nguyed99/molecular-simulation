import numpy as np
import os
from typing import List

from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.sans-serif": "Helvetica",
})

linestyle = ['solid', 'dotted', 'dashed', 'dashdot', 'loosely dotted', 'densely dotted', 'long dash with offset',
             'loosely dashed', 'densely dashed', 'loosely dashdotted', 'dashdotted', 'densely dashdotted',
             'dashdotdotted', 'loosely dashdotdotted', 'densely dashdotdotted']


def twoD_plot(solutions: List[np.array], axis: List[str],  fname: str, path,legend=None,title=None,):
    if len(solutions) > len(linestyle):
        raise ValueError('Crazy person ! How the hell do you think ppl can differentiate that many lines ?')
    plt.figure()
    x_axis, y_axis = axis
    for i,sol in enumerate(solutions):
        t, u = sol
        if legend:
            plt.plot(t,u, ls=linestyle[i], label=legend[i])
        else:
            plt.plot(t,u, ls=linestyle[i])
    plt.xlabel(x_axis); plt.ylabel(y_axis)
    if legend:
        plt.legend()
    if title:
        plt.title(title)
    plt.tight_layout()
    plt.show()
    plt.savefig(os.path.join(path, fname + '.png'))


def phasespace_trajectory_plot(solution: np.array, title: str, fname: str, path, fps=30, interval=500):
    """
    :param interval: delay between frames in terms of dt
    Return animation of trajectory  
    """
    t, x, p = solution
    fig, ax = plt.subplots()
    ln, = ax.plot([], [])
    ax.set_title(title)

    time_template = 'time = %.1fs'
    time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
    xdata, pdata = [], []
    dt = t[1]-t[0]

    def animate(i):
        xdata.append(x[i])
        pdata.append(p[i])

        ln.set_data(xdata, pdata)

        time_text.set_text(time_template % (i*dt))
        ax.relim()
        ax.autoscale_view(True,True,True)
        return ln, time_text
    
    anim = FuncAnimation(fig, animate, frames=len(t), interval=dt*interval, blit=False, repeat=False)
    anim.save(os.path.join(path, fname + '.mp4'), writer='ffmpeg', fps=fps)

