import numpy as np
from PS3_Jung.PS3 import force, verlet_chain_HO
from graphics import twoD_plot
from pathlib import Path
import os
path = Path(__file__).parent.resolve() / 'Results'
if not os.path.exists(path):
    os.makedirs(path)
## a
m = 1
k = 1
l = 1
eps = 1
tau = 1
t = (0,5000)
k_B = 1.380649*1e-23
N = 50
M = m*N
x0 = np.zeros((N,3))
x0[:,0]=np.array([i+1 for i in range(N)])
p0 = np.zeros((N,3))
p0[:,0] = np.random.normal(0,np.sqrt(m*eps),N)
dt = 0.1

t_array = np.arange(t[0], t[1], dt)
x = np.zeros((len(t_array), N, 3))
p = np.zeros((len(t_array), N, 3))


time_intervals = [(i,i+dt*10) for i in range(1,5000)]
x_intermediate, p_intermediate = verlet_chain_HO(x0,p0,time_intervals[0],dt,force,N)
x[:10,:,:] = x_intermediate
p[:10,:,:] = p_intermediate

for i in range(1,len(time_intervals)):
    time = time_intervals[i]
    p[10*i-1,:,0] = np.random.normal(0,np.sqrt(m*eps),N)
    x_intermediate, p_intermediate = verlet_chain_HO(x[10*i-1,:,:],p[10*i-1,:,:],time,dt,force,N)
    x[10*i:10*(i+1),:,:] = x_intermediate
    p[10*i:10*(i+1),:,:] = p_intermediate
# radius of gyration R_g(t) and end-to-end distance R_e(t)
x = x[:,:,0]
t = np.arange(0,5000,dt)

# r_cm = (1/M)*np.sum(m*x, axis=1)
# R_g = np.zeros(len(t))
# for i in range(len(t)):
#     R_g[i] = np.sqrt((1/M)*np.sum(m*(x[i]-r_cm[i])**2))

# R_g = np.vstack((t, R_g))
# R_e = np.vstack((t,np.abs(x[:,-1]-x[:,0])))
# twoD_plot([R_g, R_e], axis=['$t / \omega_{0}^{-1}$', '$Distance /l$'], legend=['$R_{g}$', '$R_{e}$'], fname='R_gnR_e', path=path)
# twoD_plot([R_g], axis=['$t / \omega_{0}^{-1}$', '$Distance /l$'], fname='R_g(t)', path=path)

# potential energy per particle
U = np.zeros((len(t),N))
diff = np.diff(x)
U[:,1:]=(k/2)*diff**2
U = np.vstack((t,(1/N)*np.sum(U,axis=1)))
twoD_plot([U], axis=['$t / \omega_{0}^{-1}$', '$(E_{pot}/N) / \epsilon$'], fname='U(t)', path=path)

## b

