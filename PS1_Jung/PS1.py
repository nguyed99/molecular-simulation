import os
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

from graphics import twoD_plot, phasespace_trajectory_plot


M = 1e-3 # kg
K = 0.1 # N/m
t = (0,10)
dt_1 = 1e-3
dt_2 = 1e-1
X_0 = 0
P_0 = 10e-3

def heun(x0: float, p0: float, t: tuple, dt: float, f) -> np.array:
    """
    :param t: (t0,tmax)
    :param f: force function
    """
    t = np.arange(t[0], t[1], dt)
    dt=-dt
    x = np.zeros(len(t))
    x[0] = x0

    p = np.zeros(len(t))
    p[0] = p0

    for i in range(1,len(t)):
        x_intermediate = x[i-1] + (1/2)*(dt/M)*p[i-1]
        x[i] = x_intermediate + (1/(2*M))*(p[i-1]+f(x[i-1])*dt)*dt
        p[i] = p[i-1]+f(x[i-1]+p[i-1]*dt/(2*M))*dt

    return np.vstack((t, x, p))

def verlet(x0: float, p0: float, t: tuple, dt: float, f) -> np.array:
    """
    :param t: (t0,tmax)
    """
    t = np.arange(t[0], t[1], dt)
    dt = -dt
    x = np.zeros(len(t))
    x[0] = x0

    p = np.zeros(len(t))
    p[0] = p0

    for i in range(1,len(t)):
        # x[i] = x[i-1] + p[i-1]*dt/M + (1/2)*(f/M)*(dt**2)
        x[i] = x[i-1] + p[i-1]*dt/M + (1/2)*(f(x[i-1])/M)*(dt**2)

        # p[i] = p[i-1]+ (1/2)*(f-K*x[i])*dt
        p[i] = p[i-1]+ (1/2)*(f(x[i-1])+ f(x[i]))*dt

    
    return np.vstack((t, x, p))

if __name__ == '__main__':

    path = Path(__file__).parent.resolve() / 'Results'
    if not os.path.exists(path):
        os.makedirs(path)

    ## Problem 1.1
    
    f = lambda x: -K*x


    traj_heun_dt_1 = heun(X_0, P_0, t, dt_1, f)
    traj_heun_dt_2 = heun(X_0, P_0, t, dt_2, f)
    # traj_verlet_dt_1 = verlet(X_0, P_0, t, dt_1, f)
    # traj_verlet_dt_2 = verlet(X_0, P_0, t, dt_2, f)


    # # a) part 1 - x(t), p(t)
    # twoD_plot([traj_heun_dt_1[:2,:], traj_verlet_dt_1[:2,:]],['Heun', 'Verlet'],['t','x'], 'Heun_Verlet_dt=1e-3_x(t)', 'Heun_Verlet_dt=1e-3_x(t)', path )
    # twoD_plot([traj_heun_dt_2[:2,:], traj_verlet_dt_2[:2,:]],['Heun', 'Verlet'],['t','x'], 'Heun_Verlet_dt=1e-1_x(t)', 'Heun_Verlet_dt=1e-1_x(t)', path )
    # twoD_plot([traj_heun_dt_1[[0,2],:], traj_verlet_dt_1[[0,2],:]],['Heun', 'Verlet'],['t','p'], 'Heun_Verlet_dt=1e-3_p(t)', 'Heun_Verlet_dt=1e-3_p(t)', path )
    # twoD_plot([traj_heun_dt_2[[0,2],:], traj_verlet_dt_2[[0,2],:]],['Heun', 'Verlet'],['t','p'], 'Heun_Verlet_dt=1e-1_p(t)', 'Heun_Verlet_dt=1e-1_p(t)', path )


    # # a) part 2 - trajectory plot
    # phasespace_trajectory_plot(traj_heun_dt_1, 'Heun_dt=1e-3', 'Heun_dt=1e-3', path)
    # phasespace_trajectory_plot(traj_heun_dt_2, 'Heun_dt=1e-1', 'Heun_dt=1e-1',path)
    # phasespace_trajectory_plot(traj_verlet_dt_1, 'Verlet_dt=1e-3', 'Verlet_dt=1e-3', path)
    # phasespace_trajectory_plot(traj_verlet_dt_2, 'Verlet_dt=1e-1', 'Verlet_dt=1e-1', path)

    # # b) part 1 w/ E_tot = E_kin + E_pot = (p²)/2m + (1/2)*k*x²
    # E_heun_dt_1 = np.vstack((traj_heun_dt_1[0,:],(1/(2*M))*np.square(traj_heun_dt_1[2,:]) + (1/2)*K*np.square(traj_heun_dt_1[1,:])))
    # E_heun_dt_2 = np.vstack((traj_heun_dt_2[0,:],(1/(2*M))*np.square(traj_heun_dt_2[2,:]) + (1/2)*K*np.square(traj_heun_dt_2[1,:])))
    # E_verlet_dt_1 = np.vstack((traj_verlet_dt_1[0,:],(1/(2*M))*np.square(traj_verlet_dt_1[2,:]) + (1/2)*K*np.square(traj_verlet_dt_1[1,:])))
    # E_verlet_dt_2 = np.vstack((traj_verlet_dt_2[0,:],(1/(2*M))*np.square(traj_verlet_dt_2[2,:]) + (1/2)*K*np.square(traj_verlet_dt_2[1,:])))

    # delta_E_heun_dt1_t = np.vstack((traj_heun_dt_1[0,:],np.insert(np.diff(E_heun_dt_1[1,:]),0,0)))
    # delta_E_heun_dt2_t = np.vstack((traj_heun_dt_2[0,:],np.insert(np.diff(E_heun_dt_2[1,:]),0,0)))
    # delta_E_verlet_dt1_t = np.vstack((traj_verlet_dt_1[0,:],np.insert(np.diff(E_verlet_dt_1[1,:]),0,0)))
    # delta_E_verlet_dt2_t = np.vstack((traj_verlet_dt_2[0,:],np.insert(np.diff(E_verlet_dt_2[1,:]),0,0)))

    # twoD_plot([delta_E_heun_dt1_t, delta_E_verlet_dt1_t], ['Heun', 'Verlet'], ['t','$\Delta E$'], 'Heun_Verlet_dt=1e-3_$\Delta E$(t)', 'Heun_Verlet_dt1_Delta_E(t)', path)
    # twoD_plot([delta_E_heun_dt2_t, delta_E_verlet_dt2_t], ['Heun', 'Verlet'], ['t','$\Delta E$'], 'Heun_Verlet_dt=1e-1_$\Delta E$(t)', 'Heun_Verlet_dt2_Delta_E(t)', path)


    # # b) part 2
    # delta_E_heun_dt_1 = np.vstack((list(range(len(E_heun_dt_1[1,:])-1)),1/(dt_1)*np.diff(E_heun_dt_1[1,:])))
    # delta_E_heun_dt_2 = np.vstack((list(range(len(E_heun_dt_2[1,:])-1)),1/(dt_2)*np.diff(E_heun_dt_2[1,:])))
    # delta_E_verlet_dt_1 = np.vstack((list(range(len(E_verlet_dt_1[1,:])-1)),1/(dt_1)*np.diff(E_verlet_dt_1[1,:])))
    # delta_E_verlet_dt_2 = np.vstack((list(range(len(E_verlet_dt_2[1,:])-1)),1/(dt_2)*np.diff(E_verlet_dt_2[1,:])))

    # twoD_plot([delta_E_heun_dt_1, delta_E_verlet_dt_1], ['Heun', 'Verlet'], ['$\Delta t$','$\Delta E$'], '$\Delta E$($\Delta t$)dt=1e-3', 'Delta_E(Delta_t)_dt=1e-3', path)
    # twoD_plot([delta_E_heun_dt_2, delta_E_verlet_dt_2], ['Heun', 'Verlet'], ['$\Delta t$','$\Delta E$'], '$\Delta E$($\Delta t$)dt=1e-1', 'Delta_E(Delta_t)_dt=1e-1', path)

    # delta_E_heun_dt_1_sq = np.vstack((list(range(len(E_heun_dt_1[1,:])-1)),1/(dt_1**2)*np.diff(E_heun_dt_1[1,:])))
    # delta_E_heun_dt_2_sq = np.vstack((list(range(len(E_heun_dt_2[1,:])-1)),1/(dt_2**2)*np.diff(E_heun_dt_2[1,:])))
    # delta_E_verlet_dt_1_sq = np.vstack((list(range(len(E_verlet_dt_1[1,:])-1)),1/(dt_1**2)*np.diff(E_verlet_dt_1[1,:])))
    # delta_E_verlet_dt_2_sq = np.vstack((list(range(len(E_verlet_dt_2[1,:])-1)),1/(dt_2**2)*np.diff(E_verlet_dt_2[1,:])))

    # twoD_plot([delta_E_heun_dt_1_sq, delta_E_verlet_dt_1_sq], ['Heun', 'Verlet'], ['$\Delta t^{2}$','$\Delta E$'], '$\Delta E$($\Delta t^{2}$)dt=1e-3', 'Delta_E(Delta_t^2)_dt=1e-3', path)
    # twoD_plot([delta_E_heun_dt_2_sq, delta_E_verlet_dt_2_sq], ['Heun', 'Verlet'], ['$\Delta t^{2}$','$\Delta E$'], '$\Delta E$($\Delta t^{2}$)dt=1e-1', 'Delta_E(Delta_t^2)_dt=1e-1', path)

    # # c)
    # (manually) time-inverted trajectories
    x0 = 21372.097557449062 # from (correct) forward calculation
    p0 = -589.1101949748515 # from (correct) forward calculation
    traj_heun_dt_1_inv = heun(x0, p0, t, dt_1, f)
    t1,x1,p1 = traj_heun_dt_1_inv
    traj_heun_dt_1_inv= np.vstack((np.flip(t1),x1,p1))

    traj_verlet_dt_1_inv = verlet(x0, p0, t, dt_1, f)
    t2,x2,p2 = traj_verlet_dt_1_inv
    traj_verlet_dt_1_inv= np.vstack((np.flip(t2),x2,p2))

    phasespace_trajectory_plot(traj_heun_dt_1_inv, 'Heun_inv_dt=1e-3', 'Heun_inv_dt=1e-3', path)
    phasespace_trajectory_plot(traj_verlet_dt_1_inv, 'Verlet_inv_dt=1e-3', 'Verlet_inv_dt=1e-3', path)
